import datetime
import json
import logging

import ckan.plugins as p
import ckan.plugins.toolkit as toolkit
import ckan.lib.navl.dictization_functions as df
from ckan import model

from ckanext.heroslideradmin import db


log = logging.getLogger(__name__)


hero_slider_schema = {
    'id': [p.toolkit.get_validator('ignore_empty'), unicode],
    'image_url_1': [p.toolkit.get_validator('ignore_empty'), unicode],
    'hero_text_1': [p.toolkit.get_validator('ignore_empty'), unicode],

    'image_url_2': [p.toolkit.get_validator('ignore_empty'), unicode],
    'hero_text_2': [p.toolkit.get_validator('ignore_empty'), unicode],

    'image_url_3': [p.toolkit.get_validator('ignore_empty'), unicode],
    'hero_text_3': [p.toolkit.get_validator('ignore_empty'), unicode],

    'image_url_4': [p.toolkit.get_validator('ignore_empty'), unicode],
    'hero_text_4': [p.toolkit.get_validator('ignore_empty'), unicode],

    'image_url_5': [p.toolkit.get_validator('ignore_empty'), unicode],
    'hero_text_5': [p.toolkit.get_validator('ignore_empty'), unicode],

    'created': [p.toolkit.get_validator('ignore_missing'),
                p.toolkit.get_validator('isodate')],
    'updated': [p.toolkit.get_validator('ignore_missing'),
                p.toolkit.get_validator('isodate')]
}


def _hero_slider_update(context, data_dict):
    session = context['session']

    data, errors = df.validate(data_dict, hero_slider_schema, context)

    if errors:
        raise p.toolkit.ValidationError(errors)

    out = db.Hero_Slider.get_hero_images()

    if not out:
        out = db.Hero_Slider()

    items = ['image_url_1','image_url_2','image_url_3','image_url_4','image_url_5',
             'hero_text_1','hero_text_2','hero_text_3','hero_text_4','hero_text_5']
    for item in items:
        setattr(out, item, data.get(item))

    extras = {}
    extra_keys = set(hero_slider_schema.keys()) - set(items + ['id', 'created'])
    for key in extra_keys:
        if key in data:
            extras[key] = data.get(key)
    out.extras = json.dumps(extras)

    out.modified = datetime.datetime.utcnow()
    out.save()
    session.add(out)
    session.commit()


def hero_slider_update(context, data_dict):
    try:
        p.toolkit.check_access('hero_slider_update', context, data_dict)
    except p.toolkit.NotAuthorized:
        p.toolkit.abort(401, p.toolkit._('Not authorized to see this page'))
    return _hero_slider_update(context, data_dict)


@toolkit.side_effect_free
def hero_slider_get_images(context, data_dict):
    hero = db.Hero_Slider.get_hero_images()
    if hero:
        hero = db.table_dictize(hero, context)
    return hero
