# encoding: utf-8
import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit

import actions
import auth
import db
import helpers

import logging

try:
    from ckan.common import config # CKAN 2.7 and later
except ImportError:
    from pylons import config # CKAN 2.7 and earlier


log = logging.getLogger(__name__)


class HeroSliderAdminPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IConfigurable)
    plugins.implements(plugins.ITemplateHelpers)
    plugins.implements(plugins.IAuthFunctions, inherit=True)
    plugins.implements(plugins.IRoutes, inherit=True)
    plugins.implements(plugins.IActions)

    # IConfigurer
    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'heroslideradmin')
        if toolkit.check_ckan_version(min_version='2.4'):
            toolkit.add_ckan_admin_tab(config, 'hero_slider_admin', 'Hero Slider Config')

    # IConfigurable
    def configure(self, config):
        db.init()

    # ITemplateHelpers
    def get_helpers(self):
        return {
            'hero_dataset_count': helpers.dataset_count,
            'hero_get_hero_images': helpers.get_hero_images,
            'hero_get_hero_text': helpers.get_hero_text,
            'hero_get_max_image_size': helpers.get_max_image_size,
        }

    # IAuthFunctions
    def get_auth_functions(self):
        return {
            'hero_slider_update': auth.hero_slider_update,
        }

    # IRoutes
    def before_map(self, map):
        ctrl = 'ckanext.heroslideradmin.controller:HeroSliderAdminController'
        map.connect('hero_slider_admin', '/ckan-admin/hero_slider_admin',
                    action='hero_slider_admin', ckan_icon='picture', controller=ctrl)
        map.connect('hero_slider_update', '/ckan-admin/hero_slider_update',
                    action='hero_slider_update', controller=ctrl)
        return map

    # IActions
    def get_actions(self):
        action_functions = {
            'hero_slider_update': actions.hero_slider_update,
            'hero_slider_get_images': actions.hero_slider_get_images,
        }
        return action_functions
