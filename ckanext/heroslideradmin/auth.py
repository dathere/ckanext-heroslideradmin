import ckan.plugins as p

def sysadmin(context, data_dict):
    return {'success':  False}

def anyone(context, data_dict):
    return {'success': True}

if p.toolkit.check_ckan_version(min_version='2.2'):
    anyone = p.toolkit.auth_allow_anonymous_access(anyone)

hero_slider_update = sysadmin
