# encoding: utf-8
import ckan.plugins.toolkit as toolkit

try:
    from ckan.common import config # CKAN 2.7 and later
except ImportError:
    from pylons import config # CKAN 2.7 and earlier

def dataset_count():
    """Return a count of all datasets"""
    count = 0
    result = toolkit.get_action('package_search')({}, {'rows': 1})
    if result.get('count'):
        count = result.get('count')
    return count


def get_hero_images():
    items = ['image_url_1','image_url_2','image_url_3','image_url_4','image_url_5']
    image_list = []
    result = toolkit.get_action('hero_slider_get_images')({},{})
    if result:
        for item in items:
            image_url = result.get(item)
            if image_url:
                if not image_url.startswith('/uploads') and not image_url.startswith('http'):
                    image_url = '/uploads/hero/'+image_url
                image_list.append({item:image_url})
            else:
                image_list.append({item:None})
    else:
        image_list.append({'image_url_1':'/assets/background_BixbyCreekBridge.jpg'})
        image_list.append({'image_url_2':'/assets/background_SardineLake.jpg'})
    return image_list


def get_hero_text(data):
    items = ['image_url_1','image_url_2','image_url_3','image_url_4','image_url_5']
    hero_dict = {}
    if data in items:
        postfix = data[-1:]
        result = toolkit.get_action('hero_slider_get_images')({},{})
        if result:
            hero_dict['hero_text'] = result.get('hero_text_'+postfix)
    return hero_dict


def get_max_image_size():
    max_image_size = int(config.get('ckan.max_image_size', 2))
    return max_image_size