import logging
import cgi

from pylons import config

import ckan.plugins as p
import ckan.lib.helpers as h
import ckan.lib.uploader as uploader
import ckan.logic as logic
import ckan.model as model
from ckan.common import _, request, c, g, response
from ckan.controllers.package import (PackageController, _encode_params)


class HeroSliderAdminController(PackageController):
    def hero_slider_admin(self, hero=None, data=None, errors=None, error_summary=None):
        context = {'model': model, 'session': model.Session,
               'user': c.user or c.author}
        if hero:
            hero = hero[1:]
        _hero = p.toolkit.get_action('hero_slider_get_images')(context, {})
        if _hero is None:
            _hero = {}
        if p.toolkit.request.method == 'POST' and not data:
            data = dict(p.toolkit.request.POST)
            _hero.update(data)
            try:
                p.toolkit.get_action('hero_slider_update')(context, _hero)
            except p.toolkit.ValidationError, e:
                h.flash_notice(e.error_summary)
            p.toolkit.redirect_to(p.toolkit.url_for('hero_slider_admin', org_name=org_name))

        try:
            logic.check_access('sysadmin', context, {})
        except NotAuthorized:
            abort(401, _('User not authorized to view page'))

        if not data:
            data = _hero

        errors = errors or {}
        error_summary = error_summary or {}

        print error_summary

        vars = {'data': data, 'errors': errors,
                'error_summary': error_summary, 'hero': _hero}

        return p.toolkit.render('admin/manage_hero_slider_admin.html', extra_vars=vars)


    def hero_slider_update(self, data=None, errors=None, error_summary=None):
        '''
        A ckan-admin page to manage hero slider.
        '''
        context = {'model': model, 'session': model.Session,
                   'user': c.user or c.author}

        try:
            logic.check_access('sysadmin', context, {})
        except NotAuthorized:
            abort(401, _('User not authorized to view page'))

        _hero = {}

        # We're trying to add images
        if request.method == 'POST' and not data:
            data = dict(p.toolkit.request.POST)

            # Upload images
            image_fields = ['image_url_1','image_url_2','image_url_3','image_url_4','image_url_5']
            image_url_dict = {}
            for img_field in image_fields:
                postfix = img_field[-1:]
                upload_param = 'image_upload_'+postfix
                clear_parm = 'clear_upload_'+postfix

                if upload_param in dict(p.toolkit.request.params):
                    image_upload = dict(p.toolkit.request.params)[upload_param]
                    if isinstance(image_upload, cgi.FieldStorage):
                        upload = uploader.get_uploader('hero', data[img_field])
                        upload.update_data_dict(data, img_field, upload_param, clear_parm)
                        try:
                            upload.upload(uploader.get_max_image_size())
                        except p.toolkit.ValidationError, e:
                            image_url_dict[img_field] = None
                            h.flash_error(e.error_summary)
                            ctrl = 'ckanext.heroslideradmin.controller:HeroSliderAdminController'
                            return p.toolkit.redirect_to(h.url_for(controller=ctrl,
                                                                action='hero_slider_admin'))
                        image_url_dict[img_field] = '/uploads/hero/{}'.format(upload.filename)
                    else:
                        image_url_dict[img_field] = data[img_field]
                else:
                    image_url_dict[img_field] = None

            _hero.update(data)

            for img_field in image_fields:
                postfix = img_field[-1:]
                text_field = 'hero_text_'+postfix

                _hero[img_field] = image_url_dict[img_field]
                _hero[text_field] = data.get(text_field)

            try:
                junk = p.toolkit.get_action('hero_slider_update')(context, _hero)
            except p.toolkit.ValidationError, e:
                h.flash_notice(e.error_summary)

        return p.toolkit.redirect_to(h.url_for(controller='home', action='index'))
